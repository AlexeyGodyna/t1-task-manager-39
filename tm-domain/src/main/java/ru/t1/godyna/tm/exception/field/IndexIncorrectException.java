package ru.t1.godyna.tm.exception.field;

public final class IndexIncorrectException extends AbsractFieldException {

    public IndexIncorrectException() {
        super("Error! Index is incorrect...");
    }

}
